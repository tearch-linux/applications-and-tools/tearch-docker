FROM archlinux
RUN pacman -Sy wget fish --noconfirm
RUN wget https://gitlab.com/tearch-linux/configs/tearch-mirrorlist/-/raw/master/tearch-mirrorlist -O "/etc/pacman.d/tearch-mirrorlist"
RUN wget https://gitlab.com/tearch-linux/applications-and-tools/teaiso/-/raw/master/profiles/tearch/pacman.conf -O "/etc/pacman.conf"
RUN pacman-key --init
RUN pacman-key --populate archlinux
RUN rm -f /etc/os-release
RUN pacman -Syyu --noconfirm --overwrite '*'
RUN pacman -Sy tearch-keyring tearch-mirrorlist --noconfirm
CMD /bin/fish