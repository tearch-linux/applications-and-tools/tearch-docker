# Tearch docker image

```bash
# Fetch image
docker pull registry.gitlab.com/tearch-linux/applications-and-tools/tearch-docker
# Run container
docker run -it registry.gitlab.com/tearch-linux/applications-and-tools/tearch-docker
```
